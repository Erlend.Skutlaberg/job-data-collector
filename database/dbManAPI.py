from flask import Flask, jsonify, request
from DBManager import dbManager
from dotenv import load_dotenv
import os
import json

load_dotenv()

db_config_path  = os.getenv("DB_CONFIG_PATH")

if os.path.isfile(db_config_path):
    with open(db_config_path, "r") as db_conf_file:
        db_config = json.load(db_conf_file)
else:
    print(f"Database config file path is not valid: {db_config_path}")
    exit(1)


man = dbManager()
man.initiate(db_config)
man.setUpLogger()
app = Flask(__name__)

@app.route("/create_table", methods = ["POST"])
def create_table():
    data = request.get_json()
    if "table_name" in data.keys() and "primary_keys" in data.keys() and "column_headers" in data.keys():
        table_name = data["table_name"]
        primary_keys = data["primary_keys"]
        column_headers = data["column_headers"]
        if table_name and primary_keys and column_headers:
            res = man.create_table(table_name, primary_keys, column_headers)
            response = {"data": res}
            return jsonify(response)
    return ""
    
@app.route("/add_row", methods = ["POST"])
def add_row():
    data = request.get_json()
    if "table_name" in data.keys() and "row_data" in data.keys():
        table_name = data["table_name"]
        row_data = data["row_data"]
        if table_name and row_data:
            res = man.add_row(table_name, row_data)
            response = {"data": res}
            return jsonify(response)
    return ""

@app.route("/add_rows", methods = ["POST"])
def add_rows():
    data = request.get_json()
    if "table_name" in data.keys() and "data_list" in data.keys():
        table_name = data["table_name"]
        data_list = data["data_list"]
        if table_name and data_list:
            res = man.add_rows(table_name, data_list)
            response = {"data": res}
            return jsonify(response)
    return ""
    

@app.route("/get_row", methods = ["GET"])
def get_row():
    data = request.get_json()
    if "table_name" in data.keys() and "criteria" in data.keys():
        table_name = data["table_name"]
        criteria = data["criteria"]
        if table_name and criteria:
            res = man.get_row(table_name, criteria)
            response = {"data": whereIs}
            return jsonify(response)
    return ""


@app.route("/change_column_length", methods = ["POST"])
def change_column_length():
    data = request.get_json()
    if "table_name" in data.keys() and "column_name" in data.keys() and "new_length" in data.keys():
        table_name = data["table_name"]
        column_name = data["column_name"]
        new_length = data["new_length"]
        if table_name and column_name and new_length:
            res = man.changeColumnLength(table_name, column_name, new_length)
            response = {"data": res}
            return jsonify(response)
    return ""


@app.route("/get_column", methods = ["GET"])
def get_column():
    data = request.get_json()
    if "table_name" in data.keys() and "column" in data.keys():
        table_name = data["table_name"]
        column = data["column"]
        
        if table_name and column:
            res = man.get_column(table_name, column)
            response = {"data": res}
            return jsonify(response)
    return ""


@app.route("/exists", methods = ["GET"])
def exists():
    data = request.get_json()
    if "table_name" in data.keys() and "criteria" in data.keys():
        table_name = data["table_name"]
        criteria = data["criteria"]
        
        if table_name and criteria:
            res = man.exists(table_name, criteria)
            response = {"data": res}
            return jsonify(response)
    return ""

@app.route("/delete_table", methods = ["POST"])
def delete_table():
    data = request.get_json()
    if "table_name" in data.keys():
        table_name = data["table_name"]
        
        if table_name:
            res = man.delete_table(table_name)
            response = {"data": res}
            return jsonify(response)
    return ""

@app.route("/list_tables", methods = ["GET"])
def list_tables():
    res = man.list_tables()
    response = {"data": res}
    return jsonify(response)
    


@app.route("/list_rows", methods = ["GET"])
def list_rows():
    data = request.get_json()
    order_by = None
    if "order_by" in data.keys():
        order_by = data["order_by"]
    if "table_name" in data.keys():
        table_name = data["table_name"]
    
        
        if table_name:
            res = man.list_rows(table_name, order_by)
            response = {"data": res}
            return jsonify(response)
    return ""

@app.route("/add_column", methods = ["POST"])
def add_column():
    data = request.get_json()
    if "table_name" in data.keys() and "column_name" in data.keys() and "column_type" in data.keys():
        table_name = data["table_name"]
        column_name = data["column_name"]
        column_type = data["column_type"]
        
        if table_name and column_name and column_type:
            res = man.add_column(table_name, column_name, column_type)
            response = {"data": res}
            return jsonify(response)
    return ""

@app.route("/get_headers", methods = ["GET"])
def get_headers():
    data = request.get_json()
    if "table_name" in data.keys():
        table_name = data["table_name"]
        
        if table_name:
            res = man.get_headers(table_name)
            response = {"data": res}
            return jsonify(response)
    return ""

@app.route("/delete_row", methods = ["POST"])
def delete_row():
    data = request.get_json()
    if "table_name" in data.keys() and "criteria" in data.keys():
        table_name = data["table_name"]
        criteria = data["criteria"]
        
        if table_name and criteria:
            res = man.delete_row(table_name, criteria)
            response = {"data": res}
            return jsonify(response)
    return ""

@app.route("/update_column", methods = ["POST"])
def update_column():
    data = request.get_json()
    if "table_name" in data.keys() and "criteria" in data.keys() and "column_name" in data.keys() and "new_value" in data.keys():
        table_name = data["table_name"]
        criteria = data["criteria"]
        column_name = data["column_name"]
        new_value = data["new_value"]
        
        if table_name and criteria and column_name and new_value:
            res = man.update_column(table_name, criteria, column_name, new_value)
            response = {"data": res}
            return jsonify(response)
    return ""
