from DBManager import dbManager
from MockLogger import MockLogger
l = MockLogger()
# Create a new instance of dbManager (Singleton)
manager1 = dbManager()
manager1.setUpLogger(l)

# Create a new instance of dbManager (Singleton)
manager2 = dbManager()
manager2.setUpLogger(l)

table1 = "employees"
table2 = "test"

r1 = {"JobID": "123"}
r2 = {"JobID": "455"}
r3 = {"JobID": "789"}

r4 = {"name": "\'John Doe\'", "age": "30"}
r5 = {"name": "\'Jane Smith\'", "age": "35"}
r6 = {"name": "\'Bob Johnson\'", "age": "40"}

names = [r4["name"].replace("\"", "").replace("\'", ""), r5["name"].replace("\"", "").replace("\'", ""), r6["name"].replace("\"", "").replace("\'", "")]

def test_createTable():
    # Create a table
    manager1.create_table(table1, {"ID": "SERIAL"}, {"name": "VARCHAR(100)", "age": "INT"})

    manager1.create_table(table2, {"ID": "SERIAL"}, {"JobID": "VARCHAR(16)"})
    tables = manager1.list_tables()
    assert(table1 in tables and table2 in tables)


def test_add_rows():

    manager1.add_row(table2, r1)
    manager1.add_row(table2, r2)
    manager1.add_row(table2, r3)

    
    manager1.add_row(table1, r4)
    manager1.add_row(table1, r5)
    rows1 = manager1.get_column(table1, "name")
    rows2 = manager1.get_column(table2, "JobID")


    id1 = r1["JobID"] 
    id2 = r2["JobID"] 
    id3 = r3["JobID"] 
    
    name1 = r4["name"].replace("\"", "").replace("\'", "")
    name2 = r5["name"].replace("\"", "").replace("\'", "")

    assert(name1 in rows1)
    assert(name2 in rows1)
    assert(id1 in rows2)
    assert(id2 in rows2)
    assert(id3 in rows2)

def test_manager_can_view_changes_of_another_manager():
    
    
    manager2.add_row(table1, r6) #Manager2
    name3 = r6["name"].replace("\"", "").replace("\'", "")

    rows = manager1.get_column(table1, "name") #Manager1
    assert(name3 in rows)

def test_get_column_returns_all_values():
    cols = manager1.get_column(table1, "name")
    assert(names == cols)

def test_delete_row_and_exists():
    criteria = {"name": "\'Bob Johnson\'"}
    before_deleted = manager1.exists(table1, criteria)
    assert(before_deleted == True)

    manager1.delete_row(table1, criteria)
    after_deleted = manager1.exists(table1, criteria)
    assert(after_deleted == False)

def test_get_row():
    row = manager1.get_row(table1, {"name": names[0]})
    r4_vals = [val.replace("\"", "").replace("\'", "") for val in r4.values()]
    r4_vals[1] = int(r4_vals[1])
    r4_vals.append(1)
    for val in r4_vals:
        assert(val in row)
    

test_createTable()
test_add_rows()
test_manager_can_view_changes_of_another_manager()
test_get_column_returns_all_values()
test_delete_row_and_exists()
test_get_row()