import json
import os
def get_config():
    with open("config.json", "r") as c_file:
        config = json.load(c_file)

    db_config_path = config["db_config_path"]

    if not os.path.isfile(db_config_path):
        print(f"Database config file path is not valid: {db_config_path}")
    else:
        with open(db_config_path, "r") as db_conf_file:
            db_config = json.load(db_conf_file)
        return config, db_config

    