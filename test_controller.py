import unittest
from unittest.mock import Mock, patch, call
from controller import Controller
from MockLogger import MockLogger
import datetime
import time
import config


class TestController(unittest.TestCase):

  
    def test_get_running_jobs_works_for_multiple_sites(self):
        conf, db_config, site_map = config.get_config()
        logger  = MockLogger()
        con = Controller(conf["job_headers"], conf["site_headers"], conf["sites_to_watch"], conf["site_data_types"], conf["alien_api_url"], site_map, db_config)
        con.logger = logger
        con.sites_to_watch.append("AN::EXTRA::SITE")
    
        mock = Mock(side_effect=[{"a": ["1", "2", "3"], "b": ["4", "5", "6"]}, {"a" : ["7"], "c": ["8", "9"]}])
        with patch.object(con, "get_data", return_value = ""):
            with patch.object(con.proc, "RunningJobsToDict", mock):
                rj = con.getRunningJobs()
                self.assertEqual(rj, {"a": ["1", "2", "3", "7"], "b": ["4", "5", "6"], "c": ["8", "9"]})

        

    def test_add_data_if_table_data_returned(self):
        conf, db_config, site_map = config.get_config()
        logger = MockLogger()
        con = Controller(conf["job_headers"], conf["site_headers"], conf["sites_to_watch"], conf["site_data_types"], conf["alien_api_url"], site_map, db_config)
        con.logger = logger

        example_job_row = {"starttime": "2023-09-14 12:15:56",
                        "executable": "/file",
                        "price": "1.000000000000000E+02",
                        "type": "Job",
                        "arguments": "arg",
                        "origrequirements": "req",
                        "ttl": "82000",
                        "packages": "pack",
                        "activity": "SIM", 
                        "cpucores": "8",
                        "inputfile": "f1, f2",
                        "validationcommand": "f3",
                        "requirements": "123",
                        "inputdatatype": "NONE",
                        "inputfilesitesandsize": "a::b::c,d::e::f,g::h::i,j::k::l>8008/",
                        "site": "ALICE::UiB::SLURM",
                        "workernode": "w1",
                        "endtime": "2023-09-14 14:24:28+0200",
                        "label": "DONE",
                        "jobid": "1234",
                        "memorysize": "64GB",
}
        example_site_row = {'efficiency': '1',
                        'running_jobs': '2',
                        'total_wall_time': '3',
                        'total_cpu_time': '4',
                        'job_agents_Queued': '5',
                        'job_agents_Running': '6',
                        'job_agents_Slots': '7',
                        'status_ratio_DONE': '8',
                        'status_ratio_ERROR_E': '9',
                        'status_ratio_ERROR_V': '10',
                        'status_ratio_EXPIRED': '11',
                        'largest_jobs_memory_sum': '12',
                        'rss_sum': '13',
                        'task_queue_Force_Merged_Jobs': '14',
                        'task_queue_Inserting_Jobs': '15',
                        'task_queue_Merging_Jobs': '16',
                        'task_queue_Split_Jobs': '17',
                        'task_queue_Splitting_Jobs': '18',
                        'task_queue_Waiting_Jobs': '19',
                        'site': 'site',
                        'starttime': 'time'
                        
                        }
        get_data_mock = Mock(side_effect=[{"InputFile": ["LF: someLoc.sh"]}, "trace", "1234", "output"])
        mock_create_table_data_output = Mock(side_effect=[example_job_row, example_site_row])
        with patch.object(con, "get_data", get_data_mock):
            with patch.object(con.proc, "extractInfoFromTrace", return_value = ("site", "", "time", "", "")):
                with patch.object(con, "match_site_data", return_value = {}):
                    with patch.object(con.proc, "createTableData", mock_create_table_data_output):
                        with patch.object(con.dbManager, "exists", return_value = False):
                            with patch.object(con.dbManager, "delete_row", return_value = ""):
                                with patch.object(con.dbManager, "add_row", return_value = "") as mock_add_row:
                                    con.process_and_store_data(["2923736615"], "DONE")

        mock_add_row.assert_called()
        mock_add_row.assert_has_calls([call(con.job_data_table_name, example_job_row), call(con.site_data_table_name, example_site_row)])

    def test_add_data_to_watch_and_data_table_if_status_not_done(self):
        conf, db_config, site_map = config.get_config()
        logger = MockLogger()
        con = Controller(conf["job_headers"], conf["site_headers"], conf["sites_to_watch"], conf["site_data_types"], conf["alien_api_url"], site_map, db_config)
        con.logger = logger
        example_row = {"starttime": "2023-09-14 12:15:56",
                        "executable": "/file",
                        "price": "1.000000000000000E+02",
                        "type": "Job",
                        "arguments": "arg",
                        "origrequirements": "req",
                        "ttl": "82000",
                        "packages": "pack",
                        "activity": "SIM", 
                        "cpucores": "8",
                        "inputfile": "f1, f2",
                        "validationcommand": "f3",
                        "requirements": "123",
                        "inputdatatype": "NONE",
                        "inputfilesitesandsize": "a::b::c,d::e::f,g::h::i,j::k::l>8008/",
                        "site": "ALICE::UiB::SLURM",
                        "workernode": "w1",
                        "endTime": "2023-09-14 14:24:28+0200",
                        "label": "DONE",
                        "jobid": "1234",
                        "memorysize": "64GB",
}
        get_data_mock = Mock(side_effect=[{"InputFile": ["LF: someLoc.sh"]}, "trace", "1234", "output"])
        exists_mock = Mock(side_effect=[False, True, False])
        with patch.object(con, "get_data", get_data_mock):
            with patch.object(con.proc, "extractInfoFromTrace", return_value = ("site", "", "time", "", "")):
                with patch.object(con, "match_site_data", return_value = {}):
                    with patch.object(con.proc, "createTableData", return_value = example_row):
                        with patch.object(con.dbManager, "exists", exists_mock):
                            with patch.object(con.dbManager, "delete_row", return_value = "") as mock_delete_row:
                                with patch.object(con.dbManager, "add_row", return_value = "") as mock_add_row:
                                    con.process_and_store_data(["2923736615"], "ERROR")

        self.assertEqual(mock_add_row.call_count, 3)
        mock_delete_row.assert_called()


    def test_dont_add_data_if_not_table_data(self):
        conf, db_config, site_map = config.get_config()
        logger = MockLogger()
        con = Controller(conf["job_headers"], conf["site_headers"], conf["sites_to_watch"], conf["site_data_types"], conf["alien_api_url"], site_map, db_config)
        con.logger = logger
        example_row = {}
        exists_mock = Mock(side_effect=[False, True])
        get_data_mock = Mock(side_effect=[{"InputFile": ["LF: someLoc.sh"]}, "trace", "1234", "output"])
        with patch.object(con, "get_data", get_data_mock):
            with patch.object(con.proc, "createTableData", return_value = example_row):
                with patch.object(con.dbManager, "exists", exists_mock):
                    with patch.object(con.dbManager, "delete_row", return_value = "") as mock_delete_row:
                        with patch.object(con.dbManager, "add_row", return_value = "") as mock_add_row:
                            con.process_and_store_data(["2923736615"], "ERROR")

        
        mock_delete_row.assert_not_called()
        mock_add_row.assert_not_called()


    def test_active_jobs_action_does_not_include_stored_jobs(self):
        conf, db_config, site_map = config.get_config()
        logger = MockLogger()
        con = Controller(conf["job_headers"], conf["site_headers"], conf["sites_to_watch"], conf["site_data_types"], conf["alien_api_url"], site_map, db_config)
        con.logger = logger
        stored_jobs = ["1", "2", "3"]

        with patch.object(con.dbManager, "get_column", return_value = stored_jobs):
            with patch.object(con, "process_and_store_data", return_value = "") as mock_add_to_table:
                con.activeJobsAction({"a": ["1", "2", "3", "4", "5", "6"]})

        mock_add_to_table.assert_called_with(["4", "5", "6"], "a")


    def test_watch_table_action_stores_new_submission_of_jobs_and_delete_expired_jobs(self):
        conf, db_config, site_map = config.get_config()
        logger = MockLogger()
        con = Controller(conf["job_headers"], conf["site_headers"], conf["sites_to_watch"], conf["site_data_types"], conf["alien_api_url"], site_map, db_config)
        con.logger = logger
        today = datetime.date.today()
        three_days_ago = today - datetime.timedelta(days = 3)
        eight_days_ago = today - datetime.timedelta(days=8)
        today = today.strftime('%Y-%m-%d %H:%M:%S%z')
        three_days_ago = three_days_ago.strftime('%Y-%m-%d %H:%M:%S%z')
        eight_days_ago = eight_days_ago.strftime('%Y-%m-%d %H:%M:%S%z')
      
       

        rows = [["a", three_days_ago, eight_days_ago, "123"],
                ["a", three_days_ago, eight_days_ago, "456"],
                ["b", today, today, "789"],
                ["c", today, today, "101"]]
       

        example_active_jobs = {"a": ["123", "456", "789", "101", "102"]}
        with patch.object(con.dbManager, "list_rows", return_value = rows):
            with patch.object(con, "get_data", return_value = "data"):
                with patch.object(con.proc, "extractInfoFromTrace", return_value = ("", "", "", today, "")):
                    with patch.object(con.proc, "get_end_status", return_value = "a"): 
                        with patch.object(con, "process_and_store_data", return_value = "") as mock_add_to_table:
                            with patch.object(con.dbManager, "delete_row", return_value = "") as mock_delete_row:
                                con.watchTableAction()

        #Assert with more current endtime than stored are stored again
        mock_add_to_table.assert_called_with(["123", "456"], "a")
        #Assert expired jobs are deleted

        mock_delete_row.assert_has_calls([call(con.jobs_to_watch_table_name, {"jobid": "123"}), call(con.jobs_to_watch_table_name, {"jobid": "456"})])


    def test_queue_action_waits_and_calls_actions_with_right_input(self):
        conf, db_config, site_map = config.get_config()
        logger = MockLogger()
        con = Controller(conf["job_headers"], conf["site_headers"], conf["sites_to_watch"], conf["site_data_types"], conf["alien_api_url"], site_map, db_config)
        con.logger = logger
        self.interval = 3

        activeJobsMock = {"a": ["1", "2", "3", "4"]}

        with patch.object(time, "sleep", return_value = "") as mock_time:
            with patch.object(con, "getRunningJobs", return_value = activeJobsMock):
                with patch.object(con, "activeJobsAction", return_value = "") as mock_active_jobs_action:
                    with patch.object(con, "watchTableAction", return_value = "") as mock_watch_table_action:
                        con.QueueAction(test = True)
        
        mock_time.assert_called()
        mock_active_jobs_action.assert_called_with(activeJobsMock)
        mock_watch_table_action.assert_called()

    
    def test_start_functions_creates_table_if_not_created_and_starts_queue_action(self):
        conf, db_config, site_map = config.get_config()
        logger = MockLogger()
        con = Controller(conf["job_headers"], conf["site_headers"], conf["sites_to_watch"], conf["site_data_types"], conf["alien_api_url"], site_map, db_config)
        con.logger = logger

        tables = [con.jobs_to_watch_table_name]

        with patch.object(con.dbManager, "list_tables", return_value = tables):
            with patch.object(con, "setUpDataTable", return_value = "") as mock_setup_data_table:
                with patch.object(con, "setUpWatchTable", return_value = "") as mock_setup_watch_table:
                    with patch.object(con, "setUpSiteDataTable", return_value = "") as mock_setup_site_table:
                        with patch.object(con, "QueueAction", return_value = "") as mock_queue_action:
                            con.start()

        mock_setup_data_table.assert_called()
        mock_setup_watch_table.assert_not_called()
        mock_setup_site_table.assert_called()
        mock_queue_action.assert_called()
        
    
    @patch("multiprocessing.Pool")
    def test_get_site_data_at_timestamp_dont_request_same_data_twice(self, mock_pool):
        #Mock multiprocessing pool
        fake_pool = Mock()
        fake_pool.starmap.return_value = ["html1", "html2"]
        mock_pool.return_value = fake_pool

        #Initiate controller
        conf, db_config, site_map = config.get_config()
        logger = MockLogger()
        con = Controller(conf["job_headers"], conf["site_headers"], conf["sites_to_watch"], conf["site_data_types"], conf["alien_api_url"], site_map, db_config)
        con.logger = logger
        con.site_data_types = ["mock_data_type", "mock_data_type_2"]

        time_input = "2023-09-14 14:24:16"
        time_input_within_same_hour = "2023-09-14 14:56:23"

        #Mock values
        mock_center_time_str = "2023-09-14 14:00"
        mock_center_time = datetime.datetime.strptime(mock_center_time_str, "%Y-%m-%d %H:%M")
        mock_low_time = mock_center_time - datetime.timedelta(hours = 1)
        mock_high_time = mock_center_time + datetime.timedelta(hours = 1)
        mock_value_dict = {"v1": "1", "v2": "2"}
        site = "uib"
        expected_stored_data = {mock_center_time: {con.site_data_types[0] : mock_value_dict,
                                                   con.site_data_types[1] : mock_value_dict}}
       

       
        with patch.object(con.site_collector, "get_html_data_at_time", return_value = "html") as mock_get_html:
            with patch.object(con.proc, "extract_value_from_html", return_value = mock_value_dict) as mock_extract_value_from_html:
                con.get_site_data_at_timestamp(time_input, site)
                con.get_site_data_at_timestamp(time_input_within_same_hour, site)
        

        mock_extract_value_from_html.assert_has_calls([call("html1", "2023"), call("html2", "2023")])
        assert(con.stored_site_data == expected_stored_data)

    @patch("multiprocessing.Pool")
    def test_get_site_data_at_timestamp_dont_parse_if_no_html(self, mock_pool):
        #Mock multiprocessing pool
        fake_pool = Mock()
        fake_pool.starmap.return_value = ["", ""]
        mock_pool.return_value = fake_pool
        
        conf, db_config, site_map = config.get_config()
        logger = MockLogger()
        con = Controller(conf["job_headers"], conf["site_headers"], conf["sites_to_watch"], conf["site_data_types"], conf["alien_api_url"], site_map, db_config)
        con.logger = logger
        con.site_data_types = ["mock_data_type"]


        time_input = "2023-09-14 14:24:16"

        mock_value_dict = {"v1": "1", "v2": "2"}
        site = "uib"
        with patch.object(con.site_collector, "get_html_data_at_time", return_value = ""):
            with patch.object(con.proc, "extract_value_from_html", return_value = mock_value_dict) as mock_extract_value_from_html:
                con.get_site_data_at_timestamp(time_input, site)
                
            
       
        mock_extract_value_from_html.assert_not_called()

    def test_match_site_data_returns_empty_dict_if_site_not_recognized(self):
        conf, db_config, site_map = config.get_config()
        logger = MockLogger()
        con = Controller(conf["job_headers"], conf["site_headers"], conf["sites_to_watch"], conf["site_data_types"], conf["alien_api_url"], site_map, db_config)
        con.logger = logger
        time = "2023-09-14 14:24:16"
        site = "not::recognized::site"

        with patch.object(con, "get_site_data_at_timestamp", return_value = "") as mock_get_site_data:
            val = con.match_site_data(time, site)

        mock_get_site_data.assert_not_called()
        assert(val == {})


    def test_match_site_data_treats_different_fields_correct(self):
        conf, db_config, site_map = config.get_config()
        logger = MockLogger()
        con = Controller(conf["job_headers"], conf["site_headers"], conf["sites_to_watch"], conf["site_data_types"], conf["alien_api_url"], site_map, db_config)
        con.logger = logger

        time_to_match_str = "2023-09-14 14:24:16"
        time_to_match_stripped = "2023-09-14 14:24"
        time_to_match = datetime.datetime.strptime(time_to_match_stripped, "%Y-%m-%d %H:%M")
        #Time before time_to_match
        t1 = time_to_match - datetime.timedelta(minutes = 2)
        t2 = t1 - datetime.timedelta(minutes = 2)
        t3 = t2 - datetime.timedelta(minutes = 2)
        t4 = t3 - datetime.timedelta(minutes = 2)

        #Time after time_to_match
        t5 = time_to_match + datetime.timedelta(minutes = 2)
        t6 = t5 + datetime.timedelta(minutes = 2)
        t7 = t6 + datetime.timedelta(minutes = 2)
        t8 = t7 + datetime.timedelta(minutes = 2)
        t9 = t8 + datetime.timedelta(minutes = 2)

        largest_job_one = ["1", "2", "3", "4", "5", "6", "7", "8", "9"]
        largest_job_two = ["10", "11", "12", "13", "14", "15", "16", "17", "18", "19"]
        largest_job_three = ["21", "22", "23", "24", "25", "26", "27", "28", "29"]

        additional_field_val = ["31", "32", "33", "34", "35", "36", "37", "38", "39"]

        largest_job_time_list = [t4, t3, t2, t1, time_to_match, t5, t6, t7, t8]
        rss_time_list = [t3, t2, t1, time_to_match, t5, t6, t7, t8, t9]

        mock_site_data = {"largest_job": {"one": (largest_job_time_list, largest_job_one),
                                          "two": (largest_job_time_list, largest_job_two),
                                          "three": (largest_job_time_list, largest_job_three)},
                        "rss": {"one": (rss_time_list, largest_job_one),
                                "two": (rss_time_list, largest_job_two),
                                "three": (rss_time_list, largest_job_three)},
                        "additional_field": {"UiB": (largest_job_time_list, additional_field_val),
                                             "UiO": (rss_time_list, additional_field_val)},
                        "last_field": {"Nameless": (largest_job_time_list, largest_job_one)}}
        
        match_largest_job_index = largest_job_time_list.index(time_to_match)
        match_rss_index = rss_time_list.index(time_to_match)

    
        expected_largest_job_sum = str(int(largest_job_one[match_largest_job_index]) + int(largest_job_two[match_largest_job_index]) + int(largest_job_three[match_largest_job_index]))
        expected_rss_sum = str(int(largest_job_one[match_rss_index]) + int(largest_job_two[match_rss_index]) + int(largest_job_three[match_rss_index]))
        expected_addtional_field_val_one = additional_field_val[match_largest_job_index]
        expected_addtional_field_val_two = additional_field_val[match_rss_index]
        expected_last_field_value = largest_job_one[match_largest_job_index]

        expected_output = {"largest_jobs_memory_sum": expected_largest_job_sum,
                           "rss_sum": expected_rss_sum,
                           "additional_field_UiB": expected_addtional_field_val_one,
                           "additional_field_UiO": expected_addtional_field_val_two,
                           "last_field": expected_last_field_value}
        
        with patch.object(con, "get_site_data_at_timestamp", return_value = mock_site_data) as mock_get_site_data:
            data = con.match_site_data(time_to_match_str, "ALICE::UiB::SLURM")


        mock_get_site_data.assert_called_once()
        assert(data == expected_output)
        

       

if __name__ == "__main__":
    unittest.main()





