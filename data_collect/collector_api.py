from flask import Flask, jsonify, request
from collector import JobCollector




col = JobCollector()




app = Flask(__name__)

@app.route("/jdl", methods = ["GET"])
def jdl():
    data = request.get_json()
    if "jobid" in data.keys():
        jobid = data["jobid"]
        if jobid:
            jdl = col.getJdl(jobid)
            response = {"data": jdl}
            return jsonify(response)
    return ""
    
@app.route("/trace", methods = ["GET"])
def trace():
    data = request.get_json()
    if "jobid" in data.keys():
        jobid = data["jobid"]
        if jobid:
            trace = col.getTrace(jobid)
            response = {"data": trace}
            return jsonify(response)
    return ""

@app.route("/runningjobs", methods = ["GET"])
def runningjobs():
    data = request.get_json()
    if "site" in data.keys():
        site = data["site"]

        if site:
            runningjobs = col.getRunningJobs(site)
            response = {"data": runningjobs}
            return jsonify(response)
    return ""
    

@app.route("/whereis", methods = ["GET"])
def whereis():
    data = request.get_json()
    if "file_path" in data:
        path = data["file_path"]
        if path:
            whereIs = col.getWhereIsOutput(path)
            response = {"data": whereIs}
            return jsonify(response)
    return ""


@app.route("/filesize", methods = ["GET"])
def filesize():
    data = request.get_json()
    if "file_path" in data:
        path = data["file_path"]
        if path:
            fileSize = col.getFileSize(path)
            response = {"data": fileSize}
            return jsonify(response)
    return ""

