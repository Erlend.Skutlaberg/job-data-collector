import unittest
from unittest.mock import Mock, patch
from MockLogger import MockLogger
from collector import JobCollector
from aliCollector import SiteCollector
from datetime import datetime, timedelta



class TestCollector(unittest.TestCase):

    def test_create_form_data_creates_correct_form_data(self):
        
        collector = SiteCollector()
        mock_current_date_timr_str = "2023-09-14 14:24:00"
        mock_interval_low = datetime.strptime(mock_current_date_timr_str, "%Y-%m-%d %H:%M:%S")
      
        mock_interval_high = mock_interval_low + timedelta(hours = 1)

        #Define interval.min and interval-max
        start_time_diff = datetime.now() - mock_interval_low
        start_time_hours = int(start_time_diff.total_seconds() / 3600)
        end_time_diff = datetime.now() - mock_interval_high
        end_time_hours = int(end_time_diff.total_seconds() / 3600)

        interval_min = str(start_time_hours * 3600 * 1000)
        interval_max = str(end_time_hours * 3600 * 1000)

        form_data = collector.create_form_data(mock_interval_low, mock_interval_high, "page", "", {})
        interval_low = mock_interval_low.strftime("%Y-%m-%d") + "+" + mock_interval_low.strftime("%H") + "%3A00"
        
        interval_high = mock_interval_high.strftime("%Y-%m-%d") + "+" + mock_interval_high.strftime("%H") + "%3A00"

        assert(form_data["page"] == "page")
        assert(form_data["interval_date_low"] == interval_low)
        assert(form_data["interval_date_high"] == interval_high)
        assert(form_data["interval.min"] == interval_min)
        assert(form_data["interval.max"] == interval_max)


    def test_create_form_data_only_returns_form_data_on_one_hour_interval(self):
        
        collector = SiteCollector()

        mock_current_date_timr_str = "2023-09-14 14:24:00"
        mock_interval_low = datetime.strptime(mock_current_date_timr_str, "%Y-%m-%d %H:%M:%S")
      
        mock_interval_high = mock_interval_low + timedelta(hours = 2)

        form_data = collector.create_form_data(mock_interval_low, mock_interval_high, "", "", {})

        assert(form_data == {})

    
    def test_get_html_data_at_time_returns_empty_if_datatype_not_recognized(self):
        
        collector = SiteCollector()

        mock_current_date_timr_str = "2023-09-14 14:24:00"
        mock_interval_low = datetime.strptime(mock_current_date_timr_str, "%Y-%m-%d %H:%M:%S")
        mock_interval_center = mock_interval_low + timedelta(hours = 1)
        mock_interval_high = mock_interval_low + timedelta(hours = 2)

        with patch.object(collector, "create_form_data", return_value = "") as mock_create_form_data:
            with patch.object(collector, "get_html", return_value = "") as mock_get_html:
                val = collector.get_html_data_at_time(mock_interval_low, mock_interval_center, mock_interval_high, "not_a_datatype", "UiB")
        
        mock_create_form_data.assert_not_called()
        mock_get_html.assert_not_called()
        assert(val == "")

    



if __name__ == "__main__":
    unittest.main()