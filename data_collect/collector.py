
import sys
import json
from alienpy.xrd_tools import lfn2uri, path_grid_stat
from alienpy.wb_api import SendMsg


try:
    from alienpy.wb_api import PrintDict, retf_print
    from alienpy.alien import *  # nosec PYL-W0614
except Exception:
    print("Can't load alienpy, exiting...")
    sys.exit(1)


class JobCollector():

    def __init__(self):
        #Init connection
        setup_logging()
        self.wb = InitConnection(cmdlist_func=constructCmdList)

        #Initiate logger
        self.loggerStamp = "JobCollector"

        self.logger = logging.getLogger(__name__)
        self.logger.setLevel(logging.INFO)
        
        file_handler = logging.FileHandler("collector.log")
        file_handler.setLevel(logging.INFO)
        
        formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
        file_handler.setFormatter(formatter)
        
        self.logger.addHandler(file_handler)

    

    def getJdl(self, jobId):
        try:
            res = DO_jobInfo(self.wb,  [jobId, "-jdl"])
            
            jdl_json =  res.out
            if not jdl_json:
                return
            jdl_dict = json.loads(jdl_json)
            jdl_dict["id"] = jobId
            self.logger.info(f"{self.loggerStamp}: Got JDL of job: {jobId}")
            return jdl_dict

        except Exception as e:
            self.logger.error(f"{self.loggerStamp}: Couldn't get JDL of job: {str(e)}")
            return {}


    def getTrace(self, jobId):
        try:
            res = DO_jobInfo(self.wb,  [jobId, "-trace"])
            self.logger.info(f"{self.loggerStamp}: Successfully collected trace of job: {jobId}")
            return res.out

        except Exception as e:
            self.logger.error(f"{self.loggerStamp}: Could not get trace: {str(e)}")
            return ""


    #Example site: ALICE::UiB::SLURM
    def getRunningJobs(self, site = "ALICE::UiB::SLURM"):
        try:
            res = DO_siteJobs(self.wb, [site])
            self.logger.info(f"{self.loggerStamp}: Collected active jobs!")
            #return rDict
            return res.out
        except Exception as e:
            self.logger.error(f"{self.loggerStamp}: Couldn't get active jobs: {str(e)}")
            return {}
        


    def getWhereIsOutput(self, path):
        try:
            res = SendMsg(self.wb, "whereis", [path])
            self.logger.info(f"{self.loggerStamp}: Successfully executed whereis")
            return res.out
          
        except Exception as e:
            self.logger.error(f"{self.loggerStamp}: Could not execute whereis: {e}")
            return ""
        
    def getFileSize(self, path):
        try:
            res = path_grid_stat(self.wb, path)
            self.logger.info(f"{self.loggerStamp}: Successfully collected filesize")
         
            return res.size

        except Exception as e:
            self.logger.error(f"{self.loggerStamp}: Could not collect filesize: {e}")

    def getCE(self, site):
        try:
            res = SendMsg(self.wb, "listCEs", [site])
            print(res)
            print(res.out)
        except:
            print("ERROR")



