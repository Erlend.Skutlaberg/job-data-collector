import unittest
from unittest.mock import Mock, patch
import pytest
import alienAPI 


class TestFlaskAPI(unittest.TestCase):

    def setUp(self):
        self.app = alienAPI.app
        self.client = self.app.test_client()
        

    def test_get_jdl(self):
        expected_output = {"data": "An JDL"}
        with patch.object(alienAPI.col, "getJdl", return_value = "An JDL"):
            response = self.client.get("/jdl", json = {"jobid": "123"})
      
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json, expected_output)

    def test_get_trace(self):
        expected_output = {"data": "An trace"}
        with patch.object(alienAPI.col, "getTrace", return_value = "An trace"):
            response = self.client.get("/trace", json = {"jobid": "123"})
      
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json, expected_output)

    def test_get_runningjobs(self):
        expected_output = {"data": "running jobs"}
        with patch.object(alienAPI.col, "getRunningJobs", return_value = "running jobs"):
            response = self.client.get("/runningjobs", json = {"site": "a site"})
      
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json, expected_output)

    def test_get_whereis(self):
        expected_output = {"data": "output"}
        with patch.object(alienAPI.col, "getWhereIsOutput", return_value = "output"):
            response = self.client.get("/whereis", json = {"file_path": "a path"})
      
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json, expected_output)

    def test_get_filesize(self):
        expected_output = {"data": "size"}
        with patch.object(alienAPI.col, "getFileSize", return_value = "size"):
            response = self.client.get("/filesize", json = {"file_path": "a path"})
      
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json, expected_output)

    #Test the function create_table
    def test_create_table(self):
        expected_output = {"data": "table created"}
        with patch.object(alienAPI.col, "create_table", return_value = "table created"):
            response = self.client.post("/create_table", json = {"table_name": "table", "primary_keys": "keys", "column_headers": "headers"})
        self.assertEqual(response.status_code, 200)

    #Test the function add_row
    def test_add_row(self):
        expected_output = {"data": "row added"}
        with patch.object(alienAPI.col, "add_row", return_value = "row added"):
            response = self.client.post("/add_row", json = {"table_name": "table", "row_data": "data"})
        self.assertEqual(response.status_code, 200)

    #Test the function add_rows
    def test_add_rows(self):
        expected_output = {"data": "rows added"}
        with patch.object(alienAPI.col, "add_rows", return_value = "rows added"):
            response = self.client.post("/add_rows", json = {"table_name": "table", "data_list": "data"})
        self.assertEqual(response.status_code, 200)
    
    #Test the function get_row
    def test_get_row(self):
        expected_output = {"data": "row"}
        with patch.object(alienAPI.col, "get_row", return_value = "row"):
            response = self.client.get("/get_row", json = {"table_name": "table", "criteria": "criteria"})
        self.assertEqual(response.status_code, 200)

    #Test the function change_column_length
    def test_change_column_length(self):
        expected_output = {"data": "column length changed"}
        with patch.object(alienAPI.col, "change_column_length", return_value = "column length changed"):
            response = self.client.post("/change_column_length", json = {"table_name": "table", "column_name": "column", "new_length": "length"})
        self.assertEqual(response.status_code, 200)
    
    #Test the function get_column
    def test_get_column(self):
        expected_output = {"data": "column"}
        with patch.object(alienAPI.col, "get_column", return_value = "column"):
            response = self.client.get("/get_column", json = {"table_name": "table", "column_name": "column"})
        self.assertEqual(response.status_code, 200)
    
    #Test the function exists
    def test_exists(self):
        expected_output = {"data": "exists"}
        with patch.object(alienAPI.col, "exists", return_value = "exists"):
            response = self.client.get("/exists", json = {"table_name": "table"})
        self.assertEqual(response.status_code, 200)

    #Test the function delete table
    def test_delete_table(self):
        expected_output = {"data": "table deleted"}
        with patch.object(alienAPI.col, "delete_table", return_value = "table deleted"):
            response = self.client.post("/delete_table", json = {"table_name": "table"})
        self.assertEqual(response.status_code, 200)

    #Test the function list_tables
    def test_list_tables(self):
        expected_output = {"data": "tables"}
        with patch.object(alienAPI.col, "list_tables", return_value = "tables"):
            response = self.client.get("/list_tables")
        self.assertEqual(response.status_code, 200)

    #Test the function list_rows
    def test_list_rows(self):
        expected_output = {"data": "rows"}
        with patch.object(alienAPI.col, "list_rows", return_value = "rows"):
            response = self.client.get("/list_rows", json = {"table_name": "table"})
        self.assertEqual(response.status_code, 200)
    
    #Test the function add_column
    def test_add_column(self):
        expected_output = {"data": "column added"}
        with patch.object(alienAPI.col, "add_column", return_value = "column added"):
            response = self.client.post("/add_column", json = {"table_name": "table", "column_name": "column", "column_type": "type"})
        self.assertEqual(response.status_code, 200)

    #Test the function get_headers
    def test_get_headers(self):
        expected_output = {"data": "headers"}
        with patch.object(alienAPI.col, "get_headers", return_value = "headers"):
            response = self.client.get("/get_headers")
        self.assertEqual(response.status_code, 200)

    #Test the function delete_row
    def test_delete_row(self):
        expected_output = {"data": "row deleted"}
        with patch.object(alienAPI.col, "delete_row", return_value = "row deleted"):
            response = self.client.post("/delete_row", json = {"table_name": "table", "criteria": "criteria"})
        self.assertEqual(response.status_code, 200)

    #Test the function update_column
    def test_update_column(self):
        expected_output = {"data": "column updated"}
        with patch.object(alienAPI.col, "update_column", return_value = "column updated"):
            response = self.client.post("/update_column", json = {"table_name": "table", "criteria": "criteria", "column_name": "column", "new_value": "value"})
        self.assertEqual(response.status_code, 200)

    


if __name__ == "__main__":
    unittest.main()