## The Job Collector

The Job Collector contains logic for retrieving, parsing/filtering and storing job attributes accessed via alienpy. It has a micro service architecture, which enables
system wide access to its services. The system is dependent on a CERN-issued grid certificate, which is needed to interact with alienpy.

## Configurations

The system has two assosiated configuration files: config.json, where several fields can be defined such as which grid site to retrieve job properties from (located within the repository), and dbconfig.json, which contains database configuration such as database name, username and password (located outside the repository for security reasons). These files can be utilized to quickly set up the project.

## Initiation of the program

The initiation of the program is done via the main.py script. This initiates a controller instance, starts the processes of the applicaton.
