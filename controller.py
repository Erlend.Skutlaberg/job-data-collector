import datetime
import time

import requests
import json
import logging
import os

class Controller():

    def __init__(self):
        try:    
            with open("config.json", "r") as c_file:
                config = json.load(c_file)
            
            #Initiate logger
            self.logger = logging.getLogger(__name__)
            self.logger.setLevel(logging.INFO)
            
            file_handler = logging.FileHandler("controller.log")
            file_handler.setLevel(logging.INFO)
            
            formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
            file_handler.setFormatter(formatter)
            
            self.logger.addHandler(file_handler)


            #Time from a complete run-through till the next run-through
            self.interval = config["iteration_interval"]

            #Job-table name
            self.job_data_table_name = config["job_data_table_name"]

            #Job-table column headers
            self.job_headers = config["job_headers"]

            #Sites to watch
            self.sites_to_watch = config["sites_to_watch"]
            
            #Previus start of a run-through of the application
            self.previus_execution_time = None

            #URL to alien API
            self.api_urls = config["api_urls"]

        except Exception as e:
            self.logger.error(e)
            exit(1)
     
    #Get data from alienpy through an customized API with limited functionality
    def request(self, api, endpoint, json_data, method = "GET"):
        #Extract url 
        if api in self.api_urls.keys():
            url = self.api_urls[api]
        else:
            self.logger.error(f"Could not find API: {api}")
            return "Error"

        # Request api
        try:
            if method == "GET":
                res = requests.get(url + "/" +  endpoint, json = json_data)
            elif method == "POST":
                res = requests.post(url + "/" + endpoint, json = json_data)
            
            if res.status_code == 200:
                data = json.loads(res.text)
            
                if "data" in data.keys():
                    return data["data"]
            return "Error"
        except Exception as e:
            self.logger.error(e)
            return "Error"

            
    #Get running jobs for the defined sites
    def getRunningJobs(self):
        running_jobs = {}
        for site in self.sites_to_watch:
            #Collect raw data from alienpy
            raw_output = self.request("collector_url", endpoint = "runningjobs", json_data = {"site": site})
            #Parse to dictionary
            jobs = self.request("processor_url", "RunningJobsToDict", {"running_jobs": raw_output})
            #Merge jobs from different sites by job status
            for status in jobs:
                if status not in running_jobs.keys():
                    running_jobs[status] = []
                running_jobs[status].extend(jobs[status])
        return running_jobs

    
    
    # Retrieves, processes and stores job attributes given a list of job-ids (with the same end-status) and their end status.
    def process_and_store_data(self, jobIds, status: str):
        try:
            for id in jobIds:
                #Collect job data
                raw_job_data = self.request("collector_url", endpoint = "jdl", json_data = {"jobid": id})
                trace = self.request("collector_url", endpoint = "trace", json_data = {"jobid": id})
    
                if not raw_job_data or not trace: 
                    continue
                #Parse trace
                site, workerNode, local_start_time, local_end_time, input_load_time = self.request("processor_url", "extractPropertiesFromTrace", {"trace": trace})  

                if not site or not local_start_time or not local_end_time: #Data needed to extract related site data
                    continue    
        
                #Get input files sizes and whereis command(will be parsed to find storage elements that has the inputfile in the processor)
                input_size_and_sites = ""
                if "InputFile" not in raw_job_data.keys():
                    raw_job_data["InputFile"] = []
                #Iterate over each input file and collect size and storage elements that has the file (via alienpy whereis command)
                for f in raw_job_data["InputFile"]:
                    #Collect size and whereis-output
                    f = f.split(":")
                    if len(f) <= 1: #No input files
                        continue
                    f = f[1]
                    size = self.request("collector_url", endpoint = "filesize", json_data = {"file_path": f})
                    whereIsOutput = self.request("collector_url", endpoint = "whereis", json_data = {"file_path": f})

                    if not size or not whereIsOutput:
                        continue
                    #Extract sites from whereisoutput
                    sites = self.request("processor_url", "extractSitesFromWhereIsOutput", {"whereIsOutput": whereIsOutput})
                    #Format size and sites so they can be stored in a single column (i.e. "site1,site2>1/site3,site4>2")
                    input_size_and_sites += ",".join(sites) + ">" + size + "/"
                    
                    
                #Add new properties to our current data
                raw_job_data["inputFileSitesAndSize"] = input_size_and_sites 
                raw_job_data["site"] = site 
                raw_job_data["starttime"] = local_start_time 
                raw_job_data["workerNode"] = workerNode 
                raw_job_data["endTime"] = local_end_time 
                raw_job_data["input_load_time"] = input_load_time 
                raw_job_data["label"] = status 
                raw_job_data["jobid"] = id

                #Format data and filter out the fields we want to store
                job_data = self.request("processor_url", "createTableData", {"raw_data": raw_job_data, "headers": self.job_headers})

                if not job_data or job_data == "Error": continue
                
                #Add job_data to table is it doesn"t exist
                job_data_exists = self.request("dbManager_url", "exists", {"table_name": self.job_data_table_name, "criteria": {"jobid": job_data["jobid"], "starttime": job_data["starttime"]}})
                if not job_data_exists and job_data_exists != "Error":
                    self.request("dbManager_url", "add_row", {"table_name": self.job_data_table_name, "row_data": job_data}, method = "POST")
                
        except Exception as e:
            self.logger.error(e)

    #Run an iteration of the job collector()
    def run(self):
        #Collect running jobs(jobs currently running and jobs recently runned)
        running_jobs = self.getRunningJobs() 
        collectedJobs = self.request("dbManager_url", "get_column", {"table_name": self.job_data_table_name, "column": "jobid"})
        if collectedJobs == "Error":
            return
        
        #Fiter out already stored jobs
        collectedJobs = list(collectedJobs)
        running_jobs = {key: [id for id in value if id not in collectedJobs] for key, value in running_jobs.items()}
        del collectedJobs 
        
        #Process and store jobs 
        ignore_end_status = ["RUNNING", "SAVING", "WAITING", "STARTED", "EXPIRED"]
        for end_status in running_jobs.keys():
            if end_status in ignore_end_status:
                continue
            
            newIds = running_jobs[end_status]
            #Process 100 jobs at a time
            for i in range(0 , len(newIds) , 100):
                if len(newIds) >= i + 100:
                    ids = newIds[i : i + 100]
                    
                else:
                    ids = newIds[i :]
                self.process_and_store_data(ids, end_status)
                
        
    #Queue action to run every interval
    def QueueAction(self, test = False):
        
        if test:
            count = 0
           
        while True:
            if self.previus_execution_time != None:
                # Calculate the time difference between now and last run-through
                time_difference = datetime.datetime.now() - self.previus_execution_time

                # Calculate the remaining time until next run-through
                remaining_time = datetime.timedelta(seconds=self.interval) - time_difference
                seconds = remaining_time.total_seconds()
                next_execution = datetime.datetime.now() + remaining_time
                print(f"Next execution is at: {next_execution.strftime('%Y-%m-%d %H:%M:%S')}")
                
                if seconds > 0: #Wait for next run-through
                    print("Safe to exit program")
                    time.sleep(seconds)

            #Set previus execution time to now
            self.previus_execution_time = datetime.datetime.now()
            print("Next execution in progress!")
            #Collect running jobs
            
            self.run()

            if test: # Need to exit loop when testing
                if count > 2:
                    break
                count += 1

            

    #Start the controller
    def start(self):
        try:
            tables = self.request("dbManager_url", "list_tables", {})
            if tables == "Error":
                return
            # Create job data table if it doesn't exist
            if self.job_data_table_name not in tables:
                self.setUpDataTable()

            self.QueueAction()

        except Exception as e:
            self.logger.error(e)
        

    def setUpDataTable(self):
        primaryKeys = {"jobid": "VARCHAR(16)", "starttime": "TIMESTAMP"}
        self.request("dbManager_url", "create_table", {"table_name": self.job_data_table_name, "primary_keys": primaryKeys, "column_headers": self.job_headers}, method = "POST")



