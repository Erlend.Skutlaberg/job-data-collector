from processor import Processor
from MockLogger import MockLogger
import unittest
from datetime import datetime, timedelta
from unittest.mock import Mock, patch
import bs4

exampleRJ = "1 2 3 aliprod 1 \n 4 5 6 aliprod 9 \n  7 8 9 notaliprod 8"



class TestProcessor(unittest.TestCase):
    def test_RunningJobsToDictReturnsDict(self):
        logger = MockLogger()
        processor = Processor(logger)
        rDict = processor.RunningJobsToDict(exampleRJ)

        assert(type(rDict) == dict)


    def test_RJTDreturnsOnlyAliprodUsers(self):
        logger = MockLogger()
        processor = Processor(logger)
        rDict = processor.RunningJobsToDict(exampleRJ)
        keys = list(rDict.keys())
        assert("9" not in keys)

    def test_RJTDparsesAliprodJobs(self):
        logger = MockLogger()
        processor = Processor(logger)
        rDict = processor.RunningJobsToDict(exampleRJ)
        keys = list(rDict.keys())
        assert("3" in keys and "6" in keys)

    def test_RunningJobsToDict(self):
        logger = MockLogger()
        processor = Processor(logger)

        raw_data = "job1  1 running aliprod 2\njob2   1 running  aliprod 2\njob3  1  finished  aliprod 2\n"
        expected_result = {"running": ["job1", "job2"], "finished": ["job3"]}

        assert processor.RunningJobsToDict(raw_data) == expected_result


    def test_get_worker_node(self):
        logger = MockLogger()
        processor = Processor(logger)

        line = "WorkerNode: NodeA"
        expected_result = "NodeA"

        assert processor.get_worker_node(line) == expected_result

    def test_extractPropertiesFromTrace(self):
        logger = MockLogger()
        processor = Processor(logger)

        trace = "##################################################################\n\
JobID        MasterJobID  Status      User          Name\n\
123   345   ERROR_V     aliprod       kfkdjfkjd\n\
\n\
Machine info:\n\
WorkerNode: w1\n\
Workdir: 1212\n\
Local queue info:\n\
GlobalJobId = \"c1212\"\n\
SubmitterGlobalJobId = \"v1212119767\"\n\
GlobalJobId = \"g1212129\"\n\
SubmitterGlobalJobId = \"121212121\"\n\
SLURM_JOBID: 1212\n\
SLURM_JOB_ID: 232323\n\
\n\
State info:\n\
2023-09-19 08:56:42+0200  Job 123 inserted from sdsdsd (5) [Master Job is 345]\n\
2023-09-19 13:40:20+0200  Job ASSIGNED to: r\n\
2023-09-19 13:40:25+0200  Job state transition from ASSIGNED to STARTED\n\
2023-09-19 13:40:31+0200  Job state transition from STARTED to RUNNING\n\
2023-09-19 18:33:26+0200  Job state transition from RUNNING to SAVING\n\
2023-09-19 18:33:26+0200  Job state transition from SAVING to ERROR_V\n\
2023-09-19 18:33:26+0200  Job token deletion query done for: 12\n\
2023-09-19 18:40:38+0200  Job ASSIGNED to: g\n\
2023-09-19 18:40:12+0200  Job token deletion query done for: 123\n\
2023-09-19 18:40:12+0200  Job resubmitted (back to WAITING)\n\
2023-09-19 18:40:55+0200  Job state transition from ASSIGNED to STARTED\n\
2023-09-19 18:42:42+0200  Job state transition from STARTED to RUNNING\n\
2023-09-20 00:22:27+0200  Job state transition from RUNNING to SAVING\n\
2023-09-20 00:22:33+0200  Job state transition from SAVING to ERROR_V\n\
2023-09-20 00:22:33+0200  Job token deletion query done for: 123\n\
2023-09-20 00:27:09+0200  Job ASSIGNED to: s1\n\
2023-09-20 00:27:26+0200  Job state transition from ASSIGNED to STARTED\n\
2023-09-20 00:27:41+0200  Job state transition from STARTED to RUNNING\n\
2023-09-20 00:27:09+0200  Job token deletion query done for: 123\n\
2023-09-20 00:27:09+0200  Job resubmitted (back to WAITING)\n\
2023-09-20 06:19:26+0200  Job state transition from RUNNING to SAVING\n\
2023-09-20 06:19:35+0200  Job state transition from SAVING to ERROR_V\n\
2023-09-20 06:19:35+0200  Job token deletion query done for: 123\n\
\n\
Trace info:\n\
2023-09-19 13:40:20+0200  BatchId GlobalJobId = \"c12121\"\n\
2023-09-19 13:40:20+0200  BatchId SubmitterGlobalJobId = \"vsdsdsds\"\n\
2023-09-19 13:40:20+0200  Created workdir: sdsds\n\
2023-09-19 13:40:20+0200  Running JAliEn sdsdsd\n\
2023-09-19 13:40:20+0200  Job requested 1 CPU cores to run\n\
2023-09-19 13:40:20+0200  Local disk space limit: 10000MB\n\
2023-09-19 13:40:20+0200  Virtual memory limit (JDL): 8192MB\n\
2023-09-19 13:40:20+0200  Starting JobWrapper\n\
2023-09-19 13:40:21+0200  Job asks for a TTL of 72000 seconds\n\
2023-09-19 13:40:21+0200  Support for containers detected. Will use: sdsd\n\
2023-09-19 13:40:21+0200  JobWrapper started\n\
2023-09-19 13:40:25+0200  The following OS has been detected: \"CentOS Linux 7 (Core)\"\n\
2023-09-19 13:40:25+0200  Getting InputFile: /232323 (972 B)\n\
2023-09-19 13:40:26+0200  Getting InputFile: 232323 (208.1 MB)\n\
2023-09-19 13:40:27+0200  Getting InputFile: 232323 (208.1 MB)\n\
2023-09-19 13:40:28+0200  Getting InputFile: 23232 (5.835 KB)\n\
2023-09-19 13:40:29+0200  Getting InputFile: 232323 (1.835 KB)\n\
2023-09-19 13:40:29+0200  Getting InputFile: 2323 (41.75 KB)\n\
2023-09-19 13:40:30+0200  Getting InputFile: 232323 (2.254 KB)\n\
2023-09-19 13:40:30+0200  Starting execution\n\
2023-09-19 13:40:33+0200  Using nested containers\n\
2023-09-19 18:33:22+0200  Command '.2323232323ode 5\n\
2023-09-19 18:33:22+0200  Starting validation\n\
2023-09-19 18:33:25+0200  Using nested containers\n\
2023-09-19 18:33:26+0200  Validation error cause: job exited with message: 23232323232 failed with exitcode 132329, expecting 0\n\
2023-09-19 18:33:26+0200  Validation failed. Exit code: 2\n\
2023-09-19 18:33:26+0200  Going to uploadOutputFiles(exitStatus=ERROR_V, outputDir=/232323233)\n\
2023-09-19 18:33:26+0200  Registering temporary log files in /232323. You must do 'registerOutput 123' within 24 hours of the job termination to preserve them. After this period, they are automatically deleted.\n\
2023-09-19 18:33:26+0200  Ignoring empty archive: dffdfd.zip\n\
2023-09-19 18:33:26+0200  Ignoring empty archive: fdf.zip\n\
2023-09-19 18:33:26+0200  Ignoring empty archive: sdsd.zip\n\
2023-09-19 18:33:26+0200  Uploading: log_archive to 232323233\n\
2023-09-19 18:33:26+0200  /232323232e: uploaded as requested\n\
2023-09-19 18:33:26+0200  JobWrapper has finished execution\n\
2023-09-19 18:33:29+0200  JobWrapper exit code: 0\n\
2023-09-19 18:33:29+0200  Cleaning up after execution...\n\
2023-09-19 18:40:38+0200  BatchId GlobalJobId = \"2323232\"\n\
2023-09-19 18:40:38+0200  BatchId SubmitterGlobalJobId = \"g232325\"\n\
2023-09-19 18:40:39+0200  Created workdir: /v2323233\n\
2023-09-19 18:40:39+0200  Running JAliEn J232323232p\n\
2023-09-19 18:40:39+0200  Job requested 1 CPU cores to run\n\
2023-09-19 18:40:39+0200  Local disk space limit: 23232\n\
2023-09-19 18:40:39+0200  Virtual memory limit (JDL): 2323\n\
2023-09-19 18:40:40+0200  Starting JobWrapper\n\
2023-09-19 18:40:40+0200  Job asks for a TTL of 2323 seconds\n\
2023-09-19 18:40:40+0200  Support for containers detected. Will use: 2323232\n\
2023-09-19 18:40:40+0200  JobWrapper started\n\
2023-09-19 18:40:53+0200  The following OS has been detected: \"CentOS Linux 7 (Core)\"\n\
2023-09-19 18:40:55+0200  Getting InputFile: 121212 (972 B)\n\
2023-09-19 18:40:59+0200  Getting InputFile: 2323 (208.1 MB)\n\
2023-09-19 18:41:14+0200  Getting InputFile: 23232 (208.1 MB)\n\
2023-09-19 18:41:28+0200  Getting InputFile: 232323 (5.835 KB)\n\
2023-09-19 18:42:31+0200  Getting InputFile: 232323 (1.835 KB)\n\
2023-09-19 18:42:34+0200  Getting InputFile: 232323 (41.75 KB)\n\
2023-09-19 18:42:38+0200  Getting InputFile: 2323232 (2.254 KB)\n\
2023-09-19 18:42:41+0200  Starting execution\n\
2023-09-19 18:42:45+0200  Using nested containers\n\
2023-09-20 00:22:22+0200  Command '.12121212 5\n\
2023-09-20 00:22:22+0200  Starting validation\n\
2023-09-20 00:22:25+0200  Using nested containers\n\
2023-09-20 00:22:26+0200  Validation error cause: job exited with message: 12121212ith exitcode 139, expecting 0\n\
2023-09-20 00:22:26+0200  Validation failed. Exit code: 2\n\
2023-09-20 00:22:26+0200  Going to uploadOutputFiles(exitStatus=ERROR_V, outputDir=/dfdfdfdfdf)\n\
2023-09-20 00:22:26+0200  Registering temporary log files in /232323. You must do 'registerOutput 123' within 24 hours of the job termination to preserve them. After this period, they are automatically deleted.\n\
2023-09-20 00:22:27+0200  Ignoring empty archive: 2323p\n\
2023-09-20 00:22:27+0200  Ignoring empty archive: 2323\n\
2023-09-20 00:22:27+0200  Ignoring empty archive: 3434p\n\
2023-09-20 00:22:28+0200  Uploading: log_archive to 343434\n\
2023-09-20 00:22:33+0200  232323: uploaded as requested\n\
2023-09-20 00:22:34+0200  JobWrapper has finished execution\n\
2023-09-20 00:22:38+0200  JobWrapper exit code: 0\n\
2023-09-20 00:22:38+0200  Cleaning up after execution...\n\
2023-09-20 00:27:09+0200  BatchId SLURM_JOBID: 1212\n\
2023-09-20 00:27:09+0200  BatchId SLURM_JOB_ID: 23232\n\
2023-09-20 00:27:10+0200  Created workdir: 23232\n\
2023-09-20 00:27:10+0200  Running JAliEn JobAgent/1.7.9-1 on 343434\n\
2023-09-20 00:27:10+0200  Job requested 1 CPU cores to run\n\
2023-09-20 00:27:10+0200  Local disk space limit: 10000MB\n\
2023-09-20 00:27:10+0200  Virtual memory limit (JDL): 8192MB\n\
2023-09-20 00:27:10+0200  Starting JobWrapper\n\
2023-09-20 00:27:10+0200  Job asks for a TTL of 72000 seconds\n\
2023-09-20 00:27:10+0200  Support for containers detected. Will use:343434\n\
2023-09-20 00:27:10+0200  JobWrapper started\n\
2023-09-20 00:27:26+0200  The following OS has been detected: \"CentOS Linux 7 (Core)\"\n\
2023-09-20 00:27:27+0200  Getting InputFile: qwqwqwq (972 B)\n\
2023-09-20 00:27:27+0200  Getting InputFile: wewew (208.1 MB)\n\
2023-09-20 00:27:32+0200  Getting InputFile: qwqwqw (208.1 MB)\n\
2023-09-20 00:27:38+0200  Getting InputFile: qwqwqw (5.835 KB)\n\
2023-09-20 00:27:38+0200  Getting InputFile: hdhdhd (1.835 KB)\n\
2023-09-20 00:27:39+0200  Getting InputFile: jldbfd (41.75 KB)\n\
2023-09-20 00:27:40+0200  Getting InputFile: wewew (2.254 KB)\n\
2023-09-20 00:27:41+0200  Starting execution\n\
2023-09-20 00:27:43+0200  Using nested containers\n\
23-09-20 06:19:35+0200  JobWrapper has finished execution\n\
2023-09-20 06:19:40+0200  JobWrapper exit code: 0\n\
2023-09-20 06:19:40+0200  Cleaning up after execution...\n"
        
        actual_result = processor.extractPropertiesFromTrace(trace)
      
        

        site, node, starttime, endtime, loadtime = actual_result

        assert site == "s1"
        assert node == "w1"
        assert starttime == "2023-09-20 00:27:09"
        assert endtime == "2023-09-20 06:19:40"
        assert loadtime == "14.0"


    def test_adjustTime(self):
        logger = MockLogger()
        processor = Processor(logger)

        time_str = "2022-01-01 10:00:00-0500"
        expected_result = "2022-01-01 15:00:00"

        assert processor.adjustTime(time_str) == expected_result

    def test_get_time_and_message(self):
        logger = MockLogger()
        processor = Processor(logger)

        exampleLine = "2023-09-19 13:40:20+0200  Job ASSIGNED to: ALICE::CERN::Nemesis"
        time, message = processor.get_time_and_message(exampleLine)

        assert time == "2023-09-19 13:40:20+0200" 
        assert message.strip() == "Job ASSIGNED to: ALICE::CERN::Nemesis"

    def test_can_get_worker_node(self):
        logger = MockLogger()
        processor = Processor(logger)
        top_of_trace = "JobID        MasterJobID  Status      User          Name\n\
12   23   34     aliprod       haha.sh\n\
\n\
Machine info:\n\
WorkerNode: w1\n\
Workdir: abc\n\
Local queue info:\n\
GlobalJobId = \"gsgsgs\"\n\
SubmitterGlobalJobId = \"jjsdsj\"\n\
GlobalJobId = \"ajlsfnsf\"\n\
SubmitterGlobalJobId = \"ajsfbdbf\"\n\
SLURM_JOBID: 123\n\
SLURM_JOB_ID: 456\n"
        node = ""
        traceList = top_of_trace.split("\n")
        for line in traceList:
            maybeNode = processor.get_worker_node(line)
            if maybeNode != "":
                node = maybeNode
       
        assert node == "w1"

    def test_get_time_and_message_if_trace(self):
        logger = MockLogger()
        processor = Processor(logger)

        exampleLine = "2023-09-19 13:40:20+0200  Created workdir: msg"
        
        expected_time = "2023-09-19 13:40:20+0200"
        expected_message = "msg"
        time, message = processor.get_time_and_message_if_phrase(exampleLine, 0, "created workdir")
        assert message == expected_message
        assert time == expected_time

    def test_extractSEFromWhereIsOutput(self):
        logger = MockLogger()
        processor = Processor(logger)

        exampleWhereIsOutput = "the file dpgsim.sh is in\n\
\n\
         SE => a::b::c          pfn => jlfdjfbdjbf\n\
         SE => d::e::f         pfn => djfbdbf\n\
         SE => g::h::i        pfn => djfbdfb\n\
         SE => j::k::l       pfn => djfndjofnd\n"
        
        expected_output = ["a::b::c", "d::e::f", "g::h::i", "j::k::l"]

        actual_output = processor.extractSEFromWhereIsOutput(exampleWhereIsOutput)
        
        assert expected_output == actual_output


    def test_sort_time_val_on_time_sorts_correctly(self):
        logger = MockLogger()
        processor = Processor(logger)

        time1 = datetime.now()
        time2 = time1 - timedelta(minutes = 1)
        time3 = time2 - timedelta(minutes = 1)
        time4 = time3 - timedelta(minutes = 1)

        timelist = [time2, time1, time3, time4]
        vals = [11, 22, 33, 44]

        expected_time_list = [time4, time3, time2, time1]
        expected_vals = [44, 33, 11, 22]

        sorted_time_list, sorted_vals = processor.sort_time_val_on_time(timelist, vals)

        assert(sorted_time_list == expected_time_list)
        assert(sorted_vals == expected_vals)

    def test_parse_html_time_stamp(self):
        logger = MockLogger()
        processor = Processor(logger)
        year = "2023"
        example_time_stamp = "Sep 14, 14:59"

        expected_output = "2023-09-14 14:59:00"

        actual_output = processor.parse_html_time_stamp(example_time_stamp, year)

        assert(actual_output == expected_output)

    def test_parse_overlib_string_parses_correctly(self):
        logger = MockLogger()
        processor = Processor(logger)
        example_overlib_string = "return overlib('(Sep 14, 15:06) 44.05', CAPTION, 'UiB_LHC');"
        mock_timestamp = "mock"
        expected_output = {"timestamp": mock_timestamp,
                           "value": "44.05",
                           "plot_series": "UiB_LHC"}
        
        with patch.object(processor, "parse_html_time_stamp", return_value = mock_timestamp):
            actual_output = processor.parse_area_tag(example_overlib_string, "")

        assert(actual_output == expected_output)


    def test_parse_html_groups_data_of_equal_plots_together(self):
        logger = MockLogger()
        processor = Processor(logger)

        t1 = "Sep 14, 14:59"
        t2 = "Sep 14, 14:57"
        t3 = "Sep 14, 14:55"
        t4 = "Sep 14, 14:53"

        v1 = "1"
        v2 = "2"
        v3 = "3"
        v4 = "4"

        s1 = "s1"
        s2 = "s2"
 

        area_str = "<area >"
        ex_soup = bs4.BeautifulSoup(area_str, "html.parser")

        ex_area = ex_soup.find("area")

        mock_values = Mock(side_effect=[{"timestamp": t1, "value": v1, "plot_series": s1},
                       {"timestamp": t2, "value": v2, "plot_series": s1},
                       {"timestamp": t3, "value": v3, "plot_series": s2},
                       {"timestamp": t4, "value": v4, "plot_series": s2}])
        
        expected_output = {s1: ([t1, t2], [v1, v2]),
                           s2: ([t3, t4], [v3, v4])}
        
        with patch.object(bs4.BeautifulSoup, "find_all", return_value = [ex_area, ex_area, ex_area, ex_area]) as mock_find_all:
            with patch.object(bs4.Tag, "get", return_value = "") as mock_get:
                with patch.object(processor, "parse_area_tag", mock_values) as mock_parse_area:
                    actual_output = processor.parse_html("", "")

        mock_find_all.assert_called_once()
        mock_get.assert_called()
        mock_parse_area.assert_called()

        assert(actual_output == expected_output)


    def test_transform_val_transforms_correctly(self):
        logger = MockLogger()
        processor = Processor(logger)

        ex = "1 B"
        ex1 = "1"
        ex2 = "1 K"
        ex3 = "1 KB"
        ex4 = "1 MB"
        ex5 = "1 GB"

        exp_out = "1"
        exp_out1 = "1"
        exp_out2 = "1000"
        exp_out3 = "1000"
        exp_out4 = "1000000"
        exp_out5 = "1000000000"

        ac_out = processor.transform_val(ex)
        ac_out1 = processor.transform_val(ex1)
        ac_out2 = processor.transform_val(ex2)
        ac_out3 = processor.transform_val(ex3)
        ac_out4 = processor.transform_val(ex4)
        ac_out5 = processor.transform_val(ex5)

        assert(exp_out == ac_out)
        assert(exp_out1 == ac_out1)
        assert(exp_out2 == ac_out2)
        assert(exp_out3 == ac_out3)
        assert(exp_out4 == ac_out4)
        assert(exp_out5 == ac_out5)

    def test_extract_value_return_empty_dict_if_no_val_in_html(self):
        logger = MockLogger()
        processor = Processor(logger)

        with patch.object(processor, "parse_html", return_value = {}):
            val = processor.extract_value_from_html("", "")
        
        assert(val == {})

    def test_get_end_status_returns_end_status(self):
        logger = MockLogger()
        processor = Processor(logger)

        trace = "################################################################## \n"
        trace = "JobID        MasterJobID  Status      User          Name \n "
        trace += "43434   123345   ERROR_E     usr       file \n"

        trace += "Machine info: \n"
        
        status = processor.get_end_status(trace)

        assert(status == "ERROR_E")


        







if __name__ == "__main__":
    unittest.main()
