from flask import Flask, jsonify, request
from processor import Processor

proc = Processor()




app = Flask(__name__)

@app.route("/RunningJobsToDict", methods = ["GET"])
def runningJobsToDict():
    data = request.get_json()
    if "running_jobs" in data.keys():
        running_jobs_data = data["running_jobs"]
        if running_jobs_data:
            running_jobs = proc.RunningJobsToDict(running_jobs_data)
            response = {"data": running_jobs}
            return jsonify(response)
    return ""
    
@app.route("/extractPropertiesFromTrace", methods = ["GET"])
def extractPropertiesFromTrace():
    data = request.get_json()
    if "trace" in data.keys():
        trace = data["trace"]
        if trace:
            props = proc.extractPropertiesFromTrace(trace)
            response = {"data": props}
            return jsonify(response)
    return ""

@app.route("/extractSEFromWhereIsOutput", methods = ["GET"])
def extractSitesFromWhereIsOutput():
    data = request.get_json()
    if "whereIsOutput" in data.keys():
        whereIsOutput = data["site"]
        if whereIsOutput:
            sites = proc.extractSEFromWhereIsOutput(whereIsOutput)
            response = {"data": sites}
            return jsonify(response)
    return ""
    

@app.route("/createTableData", methods = ["GET"])
def createTableData():
    data = request.get_json()
    if "raw_data" in data and "headers" in data:
        raw_data = data["raw_data"]
        headers = data["headers"]
        if raw_data and headers:
            tableData = proc.createTableData(raw_data, headers)
            response = {"data": tableData}
            return jsonify(response)
    return ""


@app.route("/toDateTime", methods = ["GET"])
def toDateTime():
    data = request.get_json()
    if "time_str" in data:
        time_str = data["time_str"]
        if time_str:
            time = proc.toDateTime(time_str)
            response = {"data": fileSize}
            return jsonify(response)
    return ""


@app.route("/getEndStatus", methods = ["GET"])
def getEndStatus():
    data = request.get_json()
    if "trace" in data:
        trace = data["trace"]
        if trace:
            status = proc.getEndStatus(trace)
            response = {"data": status}
            return jsonify(response)
    return ""
