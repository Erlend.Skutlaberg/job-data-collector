import re
from datetime import datetime, timedelta
import logging


class Processor():

    def __init__(self):
        
        self.loggerStamp = "Processor"
        self.logger = logging.getLogger(__name__)
        self.logger.setLevel(logging.INFO)
        
        file_handler = logging.FileHandler("processor.log")
        file_handler.setLevel(logging.INFO)
        
        formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
        file_handler.setFormatter(formatter)
        
        self.logger.addHandler(file_handler)
       

    def RunningJobsToDict(self, raw_data):
        try:
            joblist = raw_data.split("\n")
            joblist = joblist[:-1]
            runningJobs = {}
            
            for job in joblist:
                #Extract each field (splitted by spaces)
                fields = [field for field in job.split(" ") if field != ""]
                
                if len(fields) < 4:
                    continue
                #We only collect for aliprod
                if fields[3] != "aliprod":
                    continue
                
                #Status, jobid
                jobId = fields[0]
                status = fields[2]
                #Add to dictionary
                if status in runningJobs.keys():
                    runningJobs[status].append(jobId)
                else:
                    runningJobs[status] = [jobId]
                
            self.logger.info(f"{self.loggerStamp}: Running jobs successfully converted to dictionary")
            return runningJobs
        except Exception as e:
            self.logger.error(f"{self.loggerStamp}: Could not parse running jobs to dictionary. Exception {str(e)}")
    
    def get_time_and_message(self, line):
        lineList = line.split(" ")
        time = " ".join(lineList[:2])
        
        message = " ".join(lineList[2:])
    
        return re.sub(r'\x1b\[[0-9;]*m', '', time), message


    def get_worker_node(self, line):
        splitLine = line.split(" ")
        for i in range(len(splitLine)):
            if i+3 >= len(splitLine):
                break
            combo = " ".join(splitLine[i:i+3])
            if combo == "Running JAliEn JobAgent":
                return splitLine[i+5][:-1]
    
        return ""
    
    #Extract the time and message if the line contains a certain phrase
    def get_time_and_message_if_phrase(self, line, pos, search_phrase):
        time, message = self.get_time_and_message(line)
        messageList = message.split(":")
       
        if messageList[pos].lower().strip() == search_phrase: 
            
            return time, ":".join(messageList[1:]).strip()
        return "", ""


    def extractPropertiesFromTrace(self, trace):
        traceList = trace.split("\n")

        startTime = ""
        endTime = ""
        site = ""
        workerNode = ""

        start_load_data_time = ""

        end_load_data_time = ""
        
        
        
        j = 0
        #Find startTime and site (Found the in the next part of the trace)
        for j, line in enumerate(traceList):
            if line.split(":")[0] == "Trace info":
                break
            time, mes = self.get_time_and_message_if_phrase(line, 0, "job assigned to")
            if mes != "":
                startTime = time
                site = mes
        if not startTime:
            return "", "", "", "", ""
        traceList = traceList[j+1:] #We dont need the lines we have already checked
        startTimeDT = self.toDatetime(startTime)

        #Find latest execution in trace info (Only need the trace from the last start time and out)
        k = 0
        for k, line in enumerate(traceList):
            time, _ = self.get_time_and_message(line)
            if time:
                time = self.toDatetime(time)
                if time >= startTimeDT:
                    break
        
        traceList = traceList[k:]

        #Find workernode
        for i, line in enumerate(traceList):
            if workerNode == "":
                workerNode = self.get_worker_node(line)
                continue
            break


        #Find start and end time of loading data files
        for line in traceList:
            if start_load_data_time == "":
                start_load_data_time, _ = self.get_time_and_message_if_phrase(line, 0, "getting inputfile")
                continue
            if end_load_data_time == "":
                end_load_data_time, _ = self.get_time_and_message_if_phrase(line, 0, "starting execution")

            else:
                break
        #Find the end time (the last time we are cleaning up after execution)
        for line in traceList[-5:]:
            endTime, _ = self.get_time_and_message_if_phrase(line, 0, "cleaning up after execution...")
            if endTime != "":
                break
        
        load_time = self.time_difference(start_load_data_time, end_load_data_time)
    
        
     
        return site, workerNode, startTime.split("+")[0], endTime.split("+")[0], load_time
    
    def get_end_status(self, trace):
        trace_list = trace.split("\n")
        for i, line in enumerate(trace_list):
        
            line_split = [elem for elem in line.split(" ") if elem != ""]
           
            if len(line_split) < 3:
                continue
            if line_split[2].lower() == "status":
                break
        
        if len(trace_list) > i + 1:
            line_split = [elem for elem in trace_list[i+1].split(" ") if elem != ""]
            if len(line_split) > 3:
                return line_split[2]
        return ""


    def time_difference(self, start_time_string, end_time_string):
        if start_time_string == "" or end_time_string == "":
            return ""
        # Convert the strings to datetime objects
        start_time = datetime.strptime(start_time_string, "%Y-%m-%d %H:%M:%S%z")
        end_time = datetime.strptime(end_time_string, "%Y-%m-%d %H:%M:%S%z")

        # Calculate the time difference
        time_difference = end_time - start_time

        # Convert the time difference to seconds
        time_difference_seconds = time_difference.total_seconds()

        return str(time_difference_seconds)


    def adjustTime(self, time_str):
        # Define the format of the timestamp
        format = '%Y-%m-%d %H:%M:%S'

        # Extract the time and timezone offset
        match = re.match(r'(\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2})([-+]\d{2})(\d{2})', time_str)
        if match:
            timestamp = match.group(1)
            timezone_offset_hours = int(match.group(2))
            timezone_offset_minutes = int(match.group(3))

            # Combine the timestamp and timezone offset without the colon separator
            adjusted_time = datetime.strptime(timestamp, format) - timedelta(hours=timezone_offset_hours, minutes=timezone_offset_minutes)

            # Convert adjusted datetime object back to string
            adjusted_time_str = adjusted_time.strftime(format)
            return adjusted_time_str
        else:
            return None
    
   
    def toDatetime(self, time, format = '%Y-%m-%d %H:%M:%S%z'):
        # Convert timestamp string to datetime object
        try:
           
            dt = datetime.strptime(time, format)
            return dt
        except:
            format = '%Y-%m-%d %H:%M:%S'
            try:
                dt = datetime.strptime(time, format)
                return dt
            except ValueError:
                return None
        
    def extractSEFromWhereIsOutput(self, out):
        out = out[1:]
        outlist = out.split("\n")
        sites = []
        for line in outlist:
            splittedSite = line.split("::")
            if len(splittedSite) > 1:
                #Needs to be transformed to "::" instead of "/" later
                site = splittedSite[0].split(" ")[-1] + "::" + splittedSite[1] + "::" + splittedSite[2].split(" ")[0]
                sites.append(site)
        return sites
    
    def createTableData(self, current_data, tableHeaders):
        columns = {}
        
        #Extract and format the key-value pairs from current_data which will be stored
        keys = list(current_data.keys())
        for header in tableHeaders:
            
            if header in keys:
                data = current_data[header]

                if type(data) == type([]):
                    data = ",".join(data)
            
            else:
                data = ""
          
            columns[header] = "\'" + data + "\'"
        
        return columns


    

   